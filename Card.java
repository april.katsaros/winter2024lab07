public class Card{
	private String value;
	private String suit;
	
	public Card(String value, String suit){
		this.value = value;
		this.suit = suit;
	}
	
	public String getValue(){
		return this.value;
	}
	public String getSuit(){
		return this.suit;
	}
	
	public String toString(){
		return this.value.toUpperCase() + " of " + this.suit.toUpperCase();
	}
	
	public double calculateScore(Card card){
		double score = 0;
		
		if (card.getValue() == "Ace"){
			score += 1.0;
		}
		else if (card.getValue() == "Two"){
			score += 2.0;
		}
		else if (card.getValue() == "Three"){
			score += 3.0;
		}
		else if (card.getValue() == "Four"){
			score += 4.0;
		}
		else if (card.getValue() == "Five"){
			score += 5.0;
		}
		else if (card.getValue() == "Six"){
			score += 6.0;
		}
		else if (card.getValue() == "Seven"){
			score += 7.0;
		}
		else if (card.getValue() == "Eight"){
			score += 8.0;
		}
		else if (card.getValue() == "Nine"){
			score += 9.0;
		}
		else if (card.getValue() == "Ten"){
			score += 10.0;
		}
		else if (card.getValue() == "Jack"){
			score += 11.0;
		}
		else if (card.getValue() == "Queen"){
			score += 12.0;
		}
		else if (card.getValue() == "King"){
			score += 13.0;
		}
		
		if (card.getSuit() == "Hearts"){
			score += 0.4;
		}
		else if (card.getSuit() == "Spades"){
			score += 0.3;
		}
		else if (card.getSuit() == "Diamonds"){
			score += 0.2;
		}
		else if (card.getSuit() == "Clubs"){
			score += 0.1;
		}
		return score;
	}
}