public class SimpleWar{
	public static void main(String[] args){
		Deck deck = new Deck();
		deck.shuffle();
		
		int round = 1;
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		String winner = "";
		int winnerPoints = 0;
		
		while (deck.length() > 0){
			System.out.println("Round " + round + ":");
			double playerOneScore = 0;
			System.out.println("Player One's score before the round is: " + playerOneScore);
			double playerTwoScore = 0;
			System.out.println("Player Two's score before the round is: " + playerTwoScore);
			
			Card playerOneCard = deck.drawTopCard();
			System.out.println("Player One drew the card: " + playerOneCard);
			Card playerTwoCard = deck.drawTopCard();
			System.out.println("Player Two drew the card: " + playerTwoCard);
			
			playerOneScore = playerOneCard.calculateScore(playerOneCard);
			System.out.println("Player One's score after the round is: " + playerOneScore);
			playerTwoScore = playerTwoCard.calculateScore(playerTwoCard);
			System.out.println("Player Two's score after the round is: " + playerTwoScore);
			
			if (playerOneScore > playerTwoScore){
				System.out.println("Player One wins!");
				playerOnePoints++;
				winner = "Player One";
				winnerPoints = playerOnePoints;
				
			}
			else {
				System.out.println("Player Two wins!");
				playerTwoPoints++;
				winner = "Player One";
				winnerPoints = playerTwoPoints;
			}
			round++;
		}
		
		System.out.println("Congrats " + winner + " for winning with " + winnerPoints + " points!");
	}
}