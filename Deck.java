import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		String[] suits = {"Hearts", "Diamonds", "Spades", "Clubs"};
		//populates the deck with 4 suits, each with 13 values, for 52 total cards
		for (int i = 0; i < this.cards.length; i++){
			int amountOfValues = values.length;
			int valuesIndex = i % 13;
			int suitsIndex = i / amountOfValues;
			cards[i] = new Card(values[valuesIndex], suits[suitsIndex]);
		}
	}
	//returns the current length of the deck
	public int length(){
		return this.numberOfCards;
	}
	//returns the last Card of the deck and subtracts one from the numberOfCards
	public Card drawTopCard(){
		Card lastCard = this.cards[this.numberOfCards - 1];
		this.numberOfCards--;
		return lastCard;
	}
	//returns a String containing all the Cards in the deck
	public String toString(){
		String allCards = "";
		for (int i = 0; i < this.numberOfCards; i++){
			allCards += this.cards[i] + "\n";
		}
		return allCards;
	}
	//switches positions of two cards until the whole deck is shuffled
	public void shuffle(){
		for (int i = 0; i < this.cards.length; i++){
			//get a random number
			int randNum = rng.nextInt(this.cards.length - i) + i;
			Card swap = this.cards[randNum];
			this.cards[randNum] = this.cards[i];
			this.cards[i] = swap;
		}
	}
}